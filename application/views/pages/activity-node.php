<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#myTab a').click(function (e){
            e.preventDefault();
        $(this).tab('show');
        })
    });
		
	$(document).ready(function(){
		$("#activityformValidate").validate();
	});
	
	$(document).ready(function(){
		$("#milestoneformValidate").validate();
	});
	
	$(document).ready(function(){
		$("#deliverableformValidate").validate();
	});

</script>

<div><h3><?php  echo $row[0]->title; //var_dump($row);?></h3></div>

<div><p><?php echo $row[0]->description;?></p>
<!-- Button to trigger modal Activity-->
	<a href="#myModal-activity" role="button" class="btn" data-toggle="modal">Edit Activity</a> 
	<a href="#myModal-remove-activity" role="button" class="btn" data-toggle="modal">Remove Activity</a> 
</div><br />

<!------------------- EDIT Modal-Activity ------------------->
<div id="myModal-activity" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form action="<?php echo site_url("pages/activity_mod_update");?>" method="POST" id="activityformValidate">

 <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Edit Activity</h3>
  </div>
  <div class="modal-body">

                <label for="name"><strong>Title:</strong></label> 
                <input type="text" style="width:514px;" name="ftitle" value="<?php  echo $row[0]->title;?>" class="required" />
                
				<label>Description:</label>
				<textarea rows="4" style="width:514px;" cols="30" name="fdescription" id="message" class="required"><?php echo $row[0]->description;?></textarea>

                <input type="hidden" name="nid" value="<?php echo $_GET["nid"]; ?>" class="required" />

  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button name="submit" class="btn btn-primary">Save changes</button>
  </div>
 </form>
</div>

<!------------------- Remove Modal-Activity ------------------->
<div id="myModal-remove-activity" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form action="<?php echo site_url("pages/activity_mod_remove");?>" method="POST">

 <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Remove Activity</h3>
  </div>
  <div class="modal-body">
   

            <h1>You are about to remove this activity and all it's content such as milestones and deliverables. Are you sure you want to do this?</h1>
                <input type="hidden" name="nid" value="<?php echo $_GET["nid"]; ?>" class="required" />

  </div>
  <div class="modal-footer">
    <button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No, close it!</button>
    <button name="submit" class="btn btn-danger">Yes, do it!</button>
  </div> 
 </form>
</div>

<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#milestones">Milestones</a></li>
  <li><a href="#deliverable">Deliverable</a></li>
</ul>
 
<div class="tab-content">
 
                <!-- Mileston Section-->
        <div class="tab-pane active" id="milestones"> Milestone Information <br />
                
                <!-- Mileston View-->
                <table  class="table table-bordered">
                <th>Title</th>
                <th>Description</th>
                <th>View</th>
                        <?php 
                        $partentId = $_GET["nid"];
                        
                        $query = $this->db->query("SELECT cid, title, description FROM milestone WHERE nid ='$partentId'");
                        foreach($query->result() as $row): ?>
                        <tr>
                                <td><?php echo $row->title; ?></td>
                                <td><?php echo substr($row->description, 0,105).'...'; ?></td>
								<td><a href="<?php echo site_url("pages/milestone_node");?>/?cid=<?php echo $row->cid; ?>">Link >></a></td>
                        </tr>
                        <?php endforeach; ?>
                </table>
                 <!-- Button to trigger modal Milestone-->
                <a href="#myModal-milestone" role="button" class="btn" data-toggle="modal">ADD Milestone</a> 
        </div>
  
          <!-- Deliverable Section-->
          <div class="tab-pane" id="deliverable">Deliverable Information <br />
                <!-- Mileston View-->
                        <table  class="table table-bordered">
                        <th>Title</th>
                        <th>Description</th>
                        <th>View</th>
                                <?php 
                                $partentId = $_GET["nid"];
                                
                                $query = $this->db->query("SELECT cid, title, description FROM deliverable WHERE nid ='$partentId'");
                                foreach($query->result() as $row): ?>
                                <tr>
                                        <td><?php echo $row->title; ?></td>
                                        <td><?php echo substr($row->description, 0,105).'...'; ?></td>
                                        <td><a href="<?php echo site_url("pages/deliverable_node");?>/?nid=<?php echo $row->cid; ?>">Link >></a></td>
                                </tr>
                                <?php endforeach; ?>
                        </table>
                 <!-- Button to trigger modal Deliverable -->
                <a href="#myModal-deliverable" role="button" class="btn" data-toggle="modal">ADD Deliverable</a>
                </div>
</div>

<!------------------- ADD NEW Modal-milestone ------------------->
<div id="myModal-milestone" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form action="<?php echo site_url("pages/milestone_con_mod");?>" method="POST" id="milestoneformValidate">

 <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Add New Milestone</h3>
  </div>
  
  <div class="modal-body">
   
                <label for="name"><strong>Title:</strong></label> 
                <input type="text" style="width:514px;" name="ftitle" class="required" />
                
				<label>Description:</label>
				<textarea rows="4" style="width:514px;" cols="30" name="fdescription" id="message" class="required"></textarea>

                <input type="hidden" name="nid" value="<?php echo $_GET["nid"]; ?>" class="required" />
                
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button name="submit" class="btn btn-primary">Save changes</button>
  </div>
 </form>
</div>

<!------------------- Modal-deliverable ------------------->
<div id="myModal-deliverable" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo site_url("pages/deliverable_con_mod");?>" method="POST" id="deliverableformValidate">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Add New Deliverable</h3>
  </div>
  <div class="modal-body">
   
                 <label for="name"><strong>Title:</strong></label> 
                <input type="text" style="width:514px;" name="ftitle" value="" class="required" />
                
				<label>Description:</label>
				<textarea rows="4" style="width:514px;" cols="30" name="fdescription" id="message" class="required"></textarea>

                <input type="hidden" name="nid" value="<?php echo $_GET["nid"]; ?>" class="required" />

  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button name="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>
 
 
 
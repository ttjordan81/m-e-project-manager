<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js" type="text/javascript"></script>

<script type="text/javascript">

	$(document).ready(function(){
		$("#milestoneformValidate").validate();
	});

</script>

<!--Breadcrumb-->

<a href="<?php echo site_url("pages/activity_node");?>/?nid=<?php  echo $row[0]->nid; ?>"><< Back to Activity</a>

<div><h3>Milestone - <?php  echo $row[0]->title;  //var_dump($row);?></h3></div>

<div><p><?php echo $row[0]->description;?></p>
<!-- Button to trigger modal Milestone-->
	<a href="#myModal-Milestone" role="button" class="btn" data-toggle="modal">Edit Milestone</a> 
	<a href="#myModal-remove-milestone" role="button" class="btn" data-toggle="modal">Remove Milestone</a> 
</div><br />

<!------------------- EDIT Modal-Milestone ------------------->
<div id="myModal-Milestone" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form action="<?php echo site_url("pages/milestone_mod_update");?>" method="POST" id="milestoneformValidate">

 <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Edit Milestone</h3>
  </div>
  <div class="modal-body">

                <label for="name"><strong>Title:</strong></label> 
                <input type="text" style="width:514px;" name="ftitle" value="<?php  echo $row[0]->title;?>" class="required" />
                
				<label>Description:</label>
				<textarea rows="4" style="width:514px;" cols="30" name="fdescription" id="message" class="required"><?php echo $row[0]->description;?></textarea>

                <input type="hidden" name="cid" value="<?php echo $_GET["cid"]; ?>" class="required" />

  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button name="submit" class="btn btn-primary">Save changes</button>
  </div>
 </form>
</div>

<!------------------- Remove Modal-Milestone ------------------->
<div id="myModal-remove-milestone" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form action="<?php echo site_url("pages/milestone_mod_remove");?>" method="POST" id="milestoneformValidate">

 <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Remove Milestone</h3>
  </div>
  <div class="modal-body">
      
		<h1>You are about to remove this milestone. Are you sure you want to do this?</h1>
		<input type="hidden" name="cid" value="<?php echo $_GET["cid"]; ?>" class="required" />

  </div>
  <div class="modal-footer">
    <button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No, close it!</button>
    <button name="submit" class="btn btn-danger">Yes, do it!</button>
  </div>
 </form>
</div>
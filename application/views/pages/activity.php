<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js" type="text/javascript"></script>

<script type="text/javascript">

	$(document).ready(function(){
		$("#activityformValidate").validate();
	});

</script>


<!-- Button to trigger modal -->
<a href="#myModal" role="button" class="btn" data-toggle="modal">Add New Activity</a>
<br />
<br />
<table  class="table table-bordered">
<th>Title</th>
<th>Description</th>
<th>View</th>
    <?php 
	$query = $this->db->query('SELECT nid, title, description FROM activity');
	foreach($query->result() as $row): ?>
    <tr>
        <td><?php echo $row->title; ?></td>
		<td><?php echo substr($row->description, 0,105).'...'; ?></td>
		<td><a href="<?php echo site_url("pages/activity_node");?>/?nid=<?php echo $row->nid; ?>">Link >></a></td>
    </tr>
    <?php endforeach; ?>
</table>

<!-- Button to trigger modal -->
<a href="#myModal" role="button" class="btn" data-toggle="modal">Add New Activity</a>
 
<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form action="<?php echo site_url("pages/activiy_con_mod");?>" method="POST" id="activityformValidate">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Add New Activity</h3>
  </div>
  <div class="modal-body">
  
                <label for="name"><strong>Title:</strong></label> 
                <input type="text" style="width:514px;" name="ftitle" class="required" />
                
				<label>Description:</label>
				<textarea rows="4" style="width:514px;" cols="30" name="fdescription" id="message" class="required"></textarea>
	
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>
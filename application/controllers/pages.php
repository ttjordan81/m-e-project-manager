<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI

class Pages extends CI_Controller {
	
	function __construct(){
	   parent::__construct();
	}
	
	// Need to build out templates
	function index(){
	   //check if user login...
	if($this->session->userdata('logged_in')){
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
			
			//load home page
			$this->load->view('templates/header');
			$this->load->view('pages/home');
			$this->load->view('templates/footer');
	   
	   }else {
		 //If no session, redirect to login page
		 redirect('login', 'refresh');
	   }
	}
	
	function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}
	/*
	* Start Activity ****************************************************
	*/
	// fucntion to load view activity and load model(Model_activity).
	public function activity(){
		 //check if user login...
		if($this->session->userdata('logged_in')){
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		
		$this->load->model('Model_activity');
		$data['query'] = $this->Model_activity->activitySelect();
		
		//Load view for activity
		$this->load->view('templates/header');
		$this->load->view('pages/activity', $data);
		$this->load->view('templates/footer');
		//echo $this->Model_activity->activitySelect();
		 
		 }else {
		 //If no session, redirect to login page
		 redirect('login', 'refresh');
	   }
	}

	// function to load activity model and insert data from form.
	public function activiy_con_mod(){
		
		// GET Post information from forms.
		$title = $_POST['ftitle'];
		$description = $_POST['fdescription'];
		
		// Load model_activity & (activityInsert) function, then pass title and descrition values.
		$this->load->model('Model_activity');
		$this->Model_activity->activityInsert($title, $description);
		
		// After submiting the data echo this to return back to activity page.
		echo 'Page was Updated <a href="'.site_url("pages/activity").'">Return to Activity View</a> << TODO (CREATE VIEW)';
	}
	
	public function activity_node(){
		
		//check if user login...
		if($this->session->userdata('logged_in')){
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		
			// Get the node ID from the view then pass it to function. Model will get the data.
			if ( isset($_GET['nid']) ) {
				
				$nid = $_GET['nid'];
				// Load info from model and present in view
				$this->load->model('Model_activity');
				// Call "activitySelect" function and assign its result to a variable
				$data['row'] = $this->Model_activity->activitySelect($nid);
				
				//Load view for activity-node
				$this->load->view('templates/header');
				$this->load->view('pages/activity-node', $data);
				$this->load->view('templates/footer');
				
			} else {
				// Handle 404.
			}
		
		 }else {
		 //If no session, redirect to login page
		 redirect('login', 'refresh');
	   }	
	}
	
	public function activity_mod_update(){
		$title = $_POST['ftitle'];
		$description = $_POST['fdescription'];
		$parentId = $_POST['nid'];
		
		$this->load->model('model_activity');
		$this->model_activity->activityUpdate($title,$description,$parentId);
		
		echo 'Page was Updated <a href="'. site_url('pages/activity_node/?nid=').''.$parentId .'">Return to Activity View</a> << TODO (CREATE VIEW)';
	}

	public function activity_mod_remove(){
	
		$nid = $_POST['nid'];
		
		$this->load->model('model_activity');
		$this->model_activity->activityDelete($nid);
	
	echo 'Activiy was deleted <a href="'. site_url('pages/activity').'">Return to Activity View</a> << TODO (CREATE VIEW)';
	}

	/*
	* Start Milestone ****************************************************
	*/
	// function to load milestone model and insert data from form.
	public function milestone_con_mod(){
		
		// GET Post information from forms.
		$title = $_POST['ftitle'];
		$description = $_POST['fdescription'];
		$parentId = $_POST['nid'];
		
		// Load model_milestone & (milestoneInsert) function, then pass title and descrition values.
		$this->load->model('Model_milestone');
		$this->Model_milestone->milestoneInsert($title, $description, $parentId);
		
		// After submiting the data echo this to return back to activity page.
		echo 'Page was Updated <a href="'. site_url('pages/activity_node/?nid=').''.$parentId .'">Return to Activity View</a> << TODO (CREATE VIEW)';
	}
	// function to load milestone model.
	public function milestone_node(){
			
			//check if user login...
			if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			
				// Get the node ID from the view then pass it to function. Model will get the data.
				if ( isset($_GET['cid']) ) {
					
					$cid = $_GET['cid'];
					// Load info from model and present in view
					$this->load->model('Model_milestone');
					// Call "activitySelect" function and assign its result to a variable
					$data['row'] = $this->Model_milestone->milestoneParentSelect($cid);
					
					//Load view for activity-node
					$this->load->view('templates/header');
					$this->load->view('pages/milestone-node', $data);
					$this->load->view('templates/footer');
					
				} else {
					// Handle 404.
				}
			 }else {
			 //If no session, redirect to login page
			 redirect('login', 'refresh');
			}	
	}
	
	public function milestone_mod_update(){
		$title = $_POST['ftitle'];
		$description = $_POST['fdescription'];
		$childId = $_POST['cid'];
		
		$this->load->model('Model_milestone');
		$this->Model_milestone->milestoneUpdate($title,$description,$childId);
		
		echo 'Milestone was Updated <a href="'. site_url('pages/milestone_node/?cid=').''.$childId .'">Return to Activity View</a> << TODO (CREATE VIEW)';
	}

	/*
	* Start Deliverable ****************************************************
	*/
	// function to load deliverable model and insert data from form.
	public function deliverable_con_mod(){
		
		// GET Post information from forms.
		$title = $_POST['ftitle'];
		$description = $_POST['fdescription'];
		$parentId = $_POST['nid'];
		
		// Load model_milestone & (milestoneInsert) function, then pass title and descrition values.
		$this->load->model('Model_deliverable');
		$this->Model_deliverable->deliverableInsert($title, $description, $parentId);
		
		// After submiting the data echo this to return back to activity page.
		echo 'Page was Updated <a href="'. site_url('pages/activity_node/?nid=').''.$parentId .'">Return to Activity View</a> << TODO (CREATE VIEW)';
	}
	// function to load deliverable model.
	public function deliverable_node(){
			
			//check if user login...
			if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			
				// Get the node ID from the view then pass it to function. Model will get the data.
				if ( isset($_GET['nid']) ) {
					
					$nid = $_GET['nid'];
					// Load info from model and present in view
					$this->load->model('Model_deliverable');
					// Call "activitySelect" function and assign its result to a variable
					$data['row'] = $this->Model_deliverable->deliverableParentSelect($nid);
					
					//Load view for activity-node
					$this->load->view('templates/header');
					$this->load->view('pages/deliverable-node', $data);
					$this->load->view('templates/footer');
					
				} else {
					// Handle 404.
				}
			 }else {
			 //If no session, redirect to login page
			 redirect('login', 'refresh');
			}	
	}
}
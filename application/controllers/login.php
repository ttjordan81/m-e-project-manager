<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
*  Well I didn't have to Google far to find this... less coding for me :)
*   "Simple Login with CodeIgniter in PHP"
* 	http://www.codefactorycr.com/login-with-codeigniter-php.html
*
*/

class Login extends CI_Controller {

 function __construct(){
   parent::__construct();
 }

 function index(){
	$this->load->helper(array('form'));
	$this->load->view('templates/login_view');

 }
}


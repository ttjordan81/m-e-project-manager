<?php

/*
* This model will handle select, updates and inserts for the activity page.
*
*/
class Model_activity extends CI_Model {

	//Select
	public function activitySelect($nid = null){
		
		$title   = '';
		$description = '';
		
		function __construct(){
        // Call the Model constructor
			parent::__construct();
		}
		$query = $this->db->query("SELECT * FROM activity WHERE nid='$nid'");
		return $query->result();
	}
	
	//Insert
	public function activityInsert($title,$description){
		// Put data from form into array. Arguments will pass in the values.
		$data = array(
		   'title' => $title,
		   'description' => $description,
		);
		$this->db->insert('activity', $data); 
	}	
	
	//Uudate
	public function activityUpdate($title,$description,$parentId){
	
		$id = $parentId;
		$data = array(
			'title' => $title,
			'description' => $description,
			);

		$this->db->where('nid', $id);
		$this->db->update('activity', $data); 
	
	}
	//
	public function activityDelete($nid){
	
	$parentID = $nid;
	
	$tables = array('activity', 'milestone', 'deliverable');
	$this->db->where('nid', $parentID);
	$this->db->delete($tables);
	}


}





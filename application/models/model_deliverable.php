<?php

/*
* This model will handle select, updates and inserts for the deliverable page.
*
*/
class Model_deliverable extends CI_Model {

	//Select
	public function deliverableSelect($nid = null){
		
		$title   = '';
		$description = '';
		
		function __construct(){
        // Call the Model constructor
			parent::__construct();
		}
		$query = $this->db->query("SELECT * FROM deliverable WHERE nid='$nid'");
		return $query->result();
	}
	//Select from parent deliverable node
	public function deliverableParentSelect($nid = null){
		
		$title   = '';
		$description = '';
		
		function __construct(){
        // Call the Model constructor
			parent::__construct();
		}
		$query = $this->db->query("SELECT * FROM deliverable WHERE cid='$nid'");
		return $query->result();
	}
	
	
	
	//Insert
	public function deliverableInsert($name,$description,$parentId){
	// Put data from form into array. Arguments will pass in the values.
	$data = array(
	   'title' => $name,
	   'description' => $description,
	   'nid' => $parentId,
	);
	$this->db->insert('deliverable', $data); 
	}	
	
	//Uudate
	public function deliverableUpdate(){
	
	
	}

}
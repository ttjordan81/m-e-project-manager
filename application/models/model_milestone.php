<?php

/*
* This model will handle select, updates and inserts for the milestone page.
*
*/
class Model_milestone extends CI_Model {

	//Select
	public function milestoneSelect($nid = null){
		
		$title   = '';
		$description = '';
		
		function __construct(){
        // Call the Model constructor
			parent::__construct();
		}
		$query = $this->db->query("SELECT * FROM milestone WHERE nid='$nid'");
		return $query->result();
	}
	
	//Select parent activity node
	public function milestoneParentSelect($cid = null){
		
		$title   = '';
		$description = '';
		
		function __construct(){
        // Call the Model constructor
			parent::__construct();
		}
		$query = $this->db->query("SELECT * FROM milestone WHERE cid='$cid'");
		return $query->result();
	}
	
	//Insert
	public function milestoneInsert($name,$description,$parentId){
	// Put data from form into array. Arguments will pass in the values.
	$data = array(
	   'title' => $name,
	   'description' => $description,
	   'nid' => $parentId,
	);
	$this->db->insert('milestone', $data); 
	}	
	
	//Uudate
	public function milestoneUpdate($title,$description,$childId){
	
		$id = $childId;
		$data = array(
			'title' => $title,
			'description' => $description,
			);

		$this->db->where('cid', $id);
		$this->db->update('milestone', $data); 
	
	}

}